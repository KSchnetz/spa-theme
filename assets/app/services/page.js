admin_js_app.app.service('Page', function($rootScope, $http, $sce) {
    this.GetSlug = function (slug) {
		var req = { method: 'GET', url: localized.dir.url + '/wp-json/wp/v2/pages?slug=' + slug }
		var promise = $http(req).then(function(resp){
			var page = {};
			if (resp.data[0]) {
				page.id = resp.data[0].id;
				page.title = $sce.trustAsHtml(resp.data[0].title.rendered);
				page.content = $sce.trustAsHtml(resp.data[0].content.rendered);
			} else {
				page.error = "Page not found";
			}
			return page;
		}, function(resp){
			return resp;
		});
        return promise;
    }
});