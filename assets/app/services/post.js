admin_js_app.app.service('Post', function($rootScope, $http, $sce) {
    this.GetSlug = function (slug) {
		var req = { method: 'GET', url: localized.dir.url + '/wp-json/wp/v2/posts?_embed&slug=' + slug };
		var promise = $http(req).then(function(resp){
			var post = resp.data[0];
			var tempPost = {};
			if (post) {
				tempPost = { id: post.id, slug: post.slug, date: FormatDate(post.date), author: post._embedded.author[0].name, title: $sce.trustAsHtml(post.title.rendered), excerpt: $sce.trustAsHtml(post.excerpt.rendered), content: $sce.trustAsHtml(post.content.rendered) };
			} else { 
				tempPost.error = "Post not found"; 
			}
			return tempPost;
		}, function(resp){ return resp; });
        return promise;
    };
    this.GetRecent = function (count = 6) {
		var req = { method: 'GET', url: localized.dir.url + '/wp-json/wp/v2/posts?_embed&per_page=' + count };
		var promise = $http(req).then(function(resp){
			var posts = [];
			var tempPost = {};
			if (resp.data) {
				angular.forEach(resp.data, function(value) {
					tempPost = { id: value.id, slug: value.slug, date: FormatDate(value.date), author: value._embedded.author[0].name, title: $sce.trustAsHtml(value.title.rendered), excerpt: $sce.trustAsHtml(value.excerpt.rendered), content: $sce.trustAsHtml(value.content.rendered) };
					posts.push(tempPost);
				});
				return posts;
			} else {
				tempPost.error = "No posts found";
				return tempPost;
			}
		}, function(resp){
			return resp;
		});
        return promise;
    };
    function FormatDate(d) {
    	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    	var dt = new Date(d);
    	return months[dt.getMonth()] + " " + dt.getDate() + ", " + dt.getFullYear();
    };
});