admin_js_app.app.directive('resize', function($window) {
    return function($scope) {
    	GetSizing();
        angular.element($window).bind('resize', function() { 
        	$scope.$apply(function() { 
	        	GetSizing(); 
	        }); 
	    });
    	function GetSizing() { $scope.width = $window.innerWidth; CheckSizing($scope.width); };
		function CheckSizing(w) {
	        if (w < 768) { SetBools(true); } 
	        else if ((w >= 768) && (w < 992)) { SetBools(false, true); } 
	        else if ((w >= 992) && (w < 1200)) { SetBools(false, false, true);} 
	        else { SetBools(false, false, false, true); }
		};
	    function SetBools(xs = false, sm = false, md = false, lg = false) {
	        $scope.isXS = xs; $scope.isSM = sm; $scope.isMD = md; $scope.isLG = lg;
	    };
    };
});