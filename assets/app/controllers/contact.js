admin_js_app.app.controller('ContactController', function($rootScope, $scope, Page) {
	$rootScope.page = 2;
	$scope.loading = true;
	$rootScope.title = "SPA Theme - Contact";
	Page.GetSlug("contact").then(function(p) {
		if (p.error) {
			$scope.pageTitle = p.error;
		} else {
			$scope.pageTitle = p.title;
			$scope.pageContent = p.content;
		}
		$scope.loading = false;
	});
});