admin_js_app.app.controller('HomeController', function($rootScope, $scope, Post) {
	$rootScope.page = 1;
	$scope.loading = true;
	$rootScope.title = "SPA Theme - Home";
	$scope.posts = {};
	Post.GetRecent().then(function(p) {
		if (!p.error) {
			$scope.posts = p;
		}
		$scope.loading = false;
	});
	$scope.MiddlePost = function(i) {
		if ((i - 1) % 3 == 0) { return true; }
		return false;
	}
	$scope.LeftPost = function(i) {
		if (i % 3 == 0) { return true; }
		return false;
	}
});