admin_js_app.app.controller('PostController', function($rootScope, $scope, $stateParams, Post) {
	$rootScope.page = 0;
	$scope.loading = true;
	$scope.slug = $stateParams.slug;
	$rootScope.title = "SPA Theme - " + $scope.slug;
	$scope.post = {};
	Post.GetSlug($scope.slug).then(function(p) {
		$scope.post.error = false;
		if (p.error) {
			$scope.post.title = p.error;
			$scope.post.error = true;
		} else {
			$scope.post = p;
		}
		$scope.loading = false;
	});
});