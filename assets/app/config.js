admin_js_app.app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$provide', '$locationProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $provide, $locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode({ enabled: true });
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('root', {
        views: { 
            'header': { 
                templateUrl: localized.dir.templates + '/header.html', 
                controller:'HeaderController'
            },
            'footer': { 
                templateUrl: localized.dir.templates + '/footer.html', 
                controller:'FooterController' 
            }
        }
    })
    .state('home', {
        parent: 'root', url: '/', views: {
            '@': { 
                templateUrl: localized.dir.templates + 'home.html', 
                controller: 'HomeController' 
            }
        }
    })
    .state('contact', {
        parent: 'root', url: '/contact', views: {
            '@': { 
                templateUrl: localized.dir.templates + 'contact.html', 
                controller: 'ContactController' 
            }
        }
    })
    .state('post', {
        parent: 'root',
        url: '/post/:slug',
        views: {
            '@': { 
                templateUrl: localized.dir.templates + 'post.html', 
                controller: 'PostController' 
            }
        }
    });
    $httpProvider.interceptors.push('interceptor');
}]);