admin_js_app.app.factory('interceptor', ['$q', function($q) {
    return {
        request: function(config) {
            config.headers['X-WP-Nonce'] = localized.nonce;
            return config || $q.when(config);
        },
        requestError: function(rejection) {
            return $q.reject(rejection);
        },
        response: function(response) {
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            return $q.reject(rejection);
        }
    };
}]);