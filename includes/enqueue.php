<?php
	add_action('wp_enqueue_scripts', 'load_assets');
    function load_assets() {
    	//SCRIPTS
        wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/lib/js/jquery.min.js');
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/lib/js/bootstrap.min.js');
        wp_enqueue_script( 'angular', get_template_directory_uri() . '/assets/lib/js/angular.min.js');
        wp_enqueue_script( 'angular-router', get_template_directory_uri() . '/assets/lib/js/ui-route.min.js');
        wp_enqueue_script( 'angular-sanitize', get_template_directory_uri() . '/assets/lib/js/angular-sanitize.min.js');
        //ANGULAR APP AND DEPENDENCIES: fac - factory, svc - service, dir - directive, fil - filter, ctrl - controller
            //NOTE: ALL JS FILES BELOW SHOULD BE CONCATENATED AND MINIFIED FOR USE IN PRODUCTION
        wp_enqueue_script( 'app', get_template_directory_uri() . '/assets/app/app.js');
        wp_enqueue_script( 'app-config', get_template_directory_uri() . '/assets/app/config.js');
        wp_enqueue_script( 'app-run', get_template_directory_uri() . '/assets/app/run.js');
        wp_enqueue_script( 'fac-interceptor', get_template_directory_uri() . '/assets/app/factories/interceptor.js');
        wp_enqueue_script( 'svc-post', get_template_directory_uri() . '/assets/app/services/post.js');
        wp_enqueue_script( 'svc-page', get_template_directory_uri() . '/assets/app/services/page.js');
        wp_enqueue_script( 'dir-resize', get_template_directory_uri() . '/assets/app/directives/resize.js');
        wp_enqueue_script( 'ctrl-header', get_template_directory_uri() . '/assets/app/controllers/header.js');
        wp_enqueue_script( 'ctrl-home', get_template_directory_uri() . '/assets/app/controllers/home.js');
        wp_enqueue_script( 'ctrl-contact', get_template_directory_uri() . '/assets/app/controllers/contact.js');
        wp_enqueue_script( 'ctrl-post', get_template_directory_uri() . '/assets/app/controllers/post.js');
        wp_enqueue_script( 'ctrl-footer', get_template_directory_uri() . '/assets/app/controllers/footer.js');
        //STYLES
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/lib/css/bootstrap.min.css');
        wp_enqueue_style( 'sizer', get_template_directory_uri() . '/assets/lib/css/sizer.css');
        wp_enqueue_style( 'styles', get_template_directory_uri() . '/style.css');
        //WP VARIABLES TO ANGULAR APP
	    wp_localize_script('app', 'localized', array(
	    		'dir' => array('url' => get_home_url(),'root' => get_template_directory_uri(), 'templates' => get_template_directory_uri() . '/assets/app/templates/'),
	            'nonce' => wp_create_nonce( 'wp_rest' )
	        )
	    );
	}
?>