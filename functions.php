<?php
	/*
	 *  Author: Kenneth Schnetz, VendorFuel
	 *  URL:vendorfuel.com/spa-theme
	 *  Custom functions, support, custom post types and more.
	 */
	require_once 'includes/enqueue.php';
	add_filter('redirect_canonical', 'no_redirect');
	function no_redirect() {
	    return false;
	}
?>