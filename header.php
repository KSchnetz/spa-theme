<!doctype html>
<html class="m-t-0" ng-app="spaApp" ng-cloak>
<head>
		<base href="/">
		<title ng-bind="$root.title">SPA Theme</title>
        <link ng-href="{{rootPath}}/assets/img/favicon.ico" rel="shortcut icon">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>
		<div ui-view="header"></div>